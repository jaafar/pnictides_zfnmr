import numpy as np

def find_max_min(arrays):
    """Find max and min of a list of arrays.

    Arguments:
    arrays -- the list of arrays

    Returns:
    max and min as tuple
    """
    _max = np.nanmax(arrays[0])
    _min = np.nanmin(arrays[0])
    for a in arrays:
        if np.nanmax(a) > _max:
            _max = np.nanmax(a)
        if np.nanmin(a) < _min:
            _min = np.nanmin(a)

    return _max, _min
