%\documentclass[notitlepage]{revtex4-2}
\documentclass[11pt]{revtex4-2}
\usepackage{calc}
\usepackage{hyperref}
\usepackage{xfrac}

\usepackage{report}


\NewDocumentCommand{\Eq}{sm}{%
    \IfBooleanTF{#1}{%
        Equation~(\ref{eq:#2})%
    }{%
        Eq.~(\ref{eq:#2})%
    }%
}

\NewDocumentCommand{\Eqs}{smm}{%
    \IfBooleanTF{#1}{%
        Equations~(\ref{eq:#2}-\ref{eq:#3})%
    }{%
        Eqs.~(\ref{eq:#2}-\ref{eq:#3})%
    }%
}

\NewDocumentCommand{\Fig}{sm}{%
    \IfBooleanTF{#1}{%
        Figure~(\ref{fig:#2})%
    }{%
        Fig.~(\ref{fig:#2})%
    }%
}

\NewDocumentCommand{\Sec}{m}{%
    Section~\ref{sec:#1}%
}

\NewDocumentCommand{\App}{m}{%
    Appendix~\ref{app:#1}%
}


\begin{document}
    \title{Exact Solutions for Spin 3/2 Nuclei Perturbed by Magnetic Field}
    \date{\today}
    \author{Jaafar Ansari}

    \begin{abstract}
        The eigenenergies and eigenstates of the nuclear Hamiltonian
        of a spin 3/2 particle in the presence of a strong magnetic field
        are derived in their exact form.
        In addition, the transition frequencies
        and matrix elements of the spin angular momentum operators
        are calculated,
        which can be used in calculations
        of transition rates of first order or higher perturbation theory.
    \end{abstract}

    \maketitle

    \section{Introduction}
    Systems with spin 3/2 nuclei are frequently studied
    using a variety of spectroscopic techniques.
    One technique in particular can be performed without any extra effort
    if an electric field gradient (EFG)
    surrounding the nucleus exists,
    giving the magnetic moment a preferred alignment.
    Often referred to as zero-field nuclear magnetic resonance (ZFNMR),
    or less appropriately nuclear quadrupole resonance,
    a pure ZFNMR system of spin 3/2 is a doubly degenerate four level system.
    Therefore, only one transition exists.
    The Hamiltonian, to be introduced in \Sec{theory},
    is fully described by two parameters.
    It is therefore impossible to extract the parameters of the system
    purely from the spectra.
    But, the degeneracy can be lifted by the introduction
    of a magnetic field,
    which is often done in nuclear magnetic resonance (NMR).
    In NMR, the magnetic field is controlled in the laboratory.
    Of greater interest however, such as in more exotic systems,
    the material can supply its own magnetic field
    and can therefore be studied using ZFNMR.

    One such material was discovered in 2006.
    Iron-based superconductors, as they are now referred to,
    are an unconventional class of superconductors
    first observed over a decade ago \cite{Kamihara2006}.
    These superconductors broke all previously accepted
    understanding of superconductivity
    and high $ T_c $ superconductivity \cite{Mazin2010}.
    Some such superconductors that have been observed
    are $X$FeAsO and $Y$Fe$_2$As$_2$, where $ X $ is a rare-earth element
    and $ Y $ is an alkaline earth metal \cite{Carretta2020}.
    A remarkable feature of these superconductors
    is the existence of a phase in which
    an antiferromagnetic stripe pattern in the iron layer
    can be observed.
    This specific arrangement produces an enormous magnetic field
    from the iron dipoles at the $ ^{75}$As nuclear site.
    Since $ ^{75} $As is a spin-3/2 particle
    in the presence of a magnetic field,
    iron-based superconductors have been prime candidates
    for study using NMR \cite{Fu2012} and ZFNMR \cite{Mukada2009}.

    While the eigenenergies and eigenstates
    of spin 3/2 particles in a magnetic field
    have been studied previously,
    both as a first-order perturbation \cite{Brooker1974}
    and in its exact form \cite{Muha1983},
    the expressions presented previously can be unwieldy,
    and as a result, lose critical insight.
    In this paper, this is remedied through
    several simplification steps.
    In addition, the transition frequencies are derived and visualized.
    Lastly, tools that can be used to calculate transition rates,
    by way of Fermi's Golden Rule
    or higher order time-dependent perturbation theory,
    are also presented.

    \section{Theory}
    \label{sec:theory}
    The two main phenomena affecting the energy levels
    of a nucleus with spin $ I = 3/2 $ in a magnetic field
    are the interaction of the electric quadrupole moment of the nucleus
    with the local electric field gradient (EFG)
    and the interaction of the nuclear magnetic moment
    with the magnetic field.
    Both of these effects are discussed.

    A spin 3/2 nucleus will have a non-zero electric quadrupole moment
    because the charge distribution is not spherically symmetric.
    When the nucleus is exposed to an EFG,
    an energy splitting will arise due to the various orientations
    the magnetic moment can assume.
    The EFG can be described by a traceless, 3$\times$3 tensor:
    \begin{equation}
        V_{ij} = \left. \diffp V{r_i, r_j} \right|_{\Vb{r} = \Vb{r}_0},
    \end{equation}
    where $ V $ is the electric potential
    and $ \Vb{r}_0 $ is the position of the nucleus.
    This interaction is fully described
    by the quadrupole Hamiltonian \cite{Slichter1990}:
    \begin{equation}
        H_\mathrm{Q}
        =
        \frac{e Q}{4 I (2 I - 1)}
        \left[
            V_{zz} (3 I_z^2 - \Vb{I}^2)
            + v_1 \{I_-, I_z\}
            + v_{-1} \{I_+, I_z\}
            + v_{2} I_-^2
            + v_{-2} I_+^2
        \right],
    \end{equation}
    where $ Q $ is the electric quadrupole moment of the nucleus,
    $ \Vb{I} = (I_x, I_y, I_z) $ is the spin angular momentum operator,
    $ I_\pm $ are the spin angular momentum raising/lowering operators,
    and
    \begin{subequations}
        \begin{gather}
            v_{\pm 1} = V_{zx} \pm \I V_{zy}
            \\
            v_{\pm 2} = \frac{1}{2} (V_{xx} - V_{yy}) \pm \I V_{xy}.
        \end{gather}
    \end{subequations}
    One can define a set of principle axes,
    where $ V_{ij} = 0 $ for $ i \neq j $.
    In a scenario where the principle axes are constant in time,
    it can be advantageous to rewrite the Hamiltonian
    in the coordinates of the principle axes as follows:
    \begin{equation}
        H_\mathrm{Q}
        =
        \frac{e Q V_{zz}}{4 I (2 I - 1)}
        \left[
            3 I_z^2 - \Vb{I}^2
            + \eta (I_x^2 - I_y^2)
        \right],
    \end{equation}
    where $ \eta = (V_{xx} - V_{yy})/V_{zz} $
    is referred to as the asymmetry parameter
    of the EFG.
    If the principle axes are chosen such that
    $ \Abs{V_{zz}} \geq \Abs{V_{yy}} \geq \Abs{V_{xx}} $,
    then the asymmetry parameter is bounded: $ 0 \leq \eta \leq 1 $.

    A particle with a magnetic moment in the presence of a magnetic field
    will also experience a splitting in its energy levels
    in a phenomenon known as the Zeeman Effect.
    The Zeeman Hamiltonian is given by
    \begin{equation}
        H_\mathrm{Z} = -h \gamma \Vb{B}_\mathrm{i} \cdot \Vb{I},
    \end{equation} 
    where $ \gamma $ is the gyromagnetic ratio of the nucleus
    and $ \Vb{B}_\mathrm{i} $ is the magnetic field felt by the nucleus.

    In this investigation, we restrict ourselves
    to the principle axes frame of the EFG
    and assume that the magnetic field is oriented entirely
    in the $ \Vb*{z} $ direction of the principle axes:
    $ \Vb{B}_\mathrm{i} = B_\mathrm{i} \Vb*{z} $.
    Defining the Larmor frequency $ \nu_\mathrm{L} = \gamma B_\mathrm{i} $ and
    the quadrupole coupling constant
    $ \nu_\mathrm{Q} = 3 e Q V_{zz}/[4 I (2 I - 1) h] $,
    the combined Zeeman plus quadrupole Hamiltonian
    for a spin-3/2 nucleus is then given by
    \begin{equation}
        H = -h \nu_\mathrm{L} I_z
            + \frac{1}{3} \nu_\mathrm{Q} h
              \left( 3 I_z^2 - \Vb{I}^2 + \eta (I_x^2 - I_y^2) \right).
        \label{eq:H}
    \end{equation}

    \section{Calculations}

    \subsection{Eigenenergies and Eigenstates}
    We begin by finding the eigenvalues and eigenvectors
    of the combined Hamiltonian given in \Eq{H}.
    To facilitate this,
    the Hamiltonian is first represented in the basis
    of the eigenstates of the spin-3/2 quadrupole Hamiltonian.
    Afterwards, the basis is changed back to the eigenstates of $ I_z $.
    For shorthand, the eigenstates of $ I_z $
    will be referred to as the ``$ m $-basis''
    and will be denoted by $ \ket{m} $;
    the eigenstates of the spin-3/2 quadrupole Hamiltonian
    will be referred to as the ``q-basis''
    and denoted by $ \ket{m'} $.

    The eigenenergies and eigenstates
    of the spin-3/2 Hamiltonian \cite{Brooker1974} are given by
    \begin{subequations}
        \begin{gather}
            \ket{\pm 3/2'}
            = \cos\frac{D}{2} \ket{\pm 3/2}
            + \sin\frac{D}{2} \ket{\mp 1/2},\quad
            E_{\pm 3/2'} = h \nu_\mathrm{Q} \rho
            \\
            \ket{\pm 1/2'}
            = \cos\frac{D}{2} \ket{\pm 1/2}
            - \sin\frac{D}{2} \ket{\mp 3/2},\quad
            E_{\pm 1/2'} = -h \nu_\mathrm{Q} \rho,
        \end{gather}
        \label{eq:qbasis}
    \end{subequations}
    where $ \sin(D/2) = \sqrt{(\rho - 1)/(2 \rho)} $
    and $ \rho = \sqrt{1 + \eta^2/3} $.
    In this notation, the matrix elements of the Hamiltonian are
    \begin{equation}
        \braket{n'|H|m'} = -h \nu_\mathrm{L} \braket{n'|I_z|m'}
                           + E_{m'} \delta_{n' m'}.
    \end{equation}
    A further simplification would be to introduce
    a unitless quantity $ \alpha = \nu_\mathrm{L}/(\nu_\mathrm{Q} \rho) $
    and a reduced Hamiltonian
    $ \tilde{H} $ defined by
    \begin{equation}
        \braket{n'|\tilde{H}|m'} \equiv
        \Braket{n'|\frac{H}{h \nu_\mathrm{Q} \rho}|m'}
        = -\alpha \braket{n'|I_z|m'}
          + \frac{E_{m'}}{\Abs{E_{m'}}} \delta_{n' m'}.
    \end{equation}
    Of course, in the q-basis the second term is a diagonal matrix
    with doubly-degenerate eigenvalues $ \pm 1 $.
    The first term is $ I_z $ represented in the q-basis.
    The details for working it out are left in \App{IzQ}.
    The final result is
    \begin{align}
        \tilde{H}
        &\underset{\text{q-basis}}{=}
        -\alpha
        \begin{pmatrix}
            \displaystyle \cos D + \frac{1}{2} & 0 & -\sin D & 0
            \\
            0 & \displaystyle \cos D - \frac{1}{2} & 0 & \sin D
            \\
            -\sin D & 0 & \displaystyle - \cos D + \frac{1}{2} & 0
            \\
            0 & \sin D & 0 & \displaystyle - \cos D -\frac{1}{2}
        \end{pmatrix}
        +
        \begin{pmatrix}
            1 & 0 & 0 & 0
            \\
            0 & -1 & 0 & 0
            \\
            0 & 0 & -1 & 0
            \\
            0 & 0 & 0 & 1
        \end{pmatrix}
        \notag
        \\
        &
        \hspace{8.5pt}=
        \begin{pmatrix}
            \displaystyle
            -\alpha \cos D - \frac{\alpha}{2} + 1 & 0 & \alpha \sin D & 0
            \\
            0 & \displaystyle -\alpha \cos D + \frac{\alpha}{2} - 1
            & 0 & -\alpha \sin D
            \\
            \alpha \sin D & 0
            & \displaystyle \alpha \cos D - \frac{\alpha}{2} - 1 & 0
            \\
            0 & -\alpha \sin D & 0
            & \displaystyle \alpha \cos D + \frac{\alpha}{2} + 1
        \end{pmatrix}.
    \end{align}
    Changing to the $ m $-basis is straightforward with a unitary operator
    given by
    \begin{equation}
        U = \sum_m \ket{m} \bra{m'}
        =
        \begin{pmatrix}
            \cos(D/2) & 0 & \sin(D/2) & 0
            \\
            0 & \cos(D/2) & 0 & -\sin(D/2)
            \\
            -\sin(D/2) & 0 & \cos(D/2) & 0
            \\
            0 & \sin(D/2) & 0 & \cos(D/2)
        \end{pmatrix}.
    \end{equation}
    The Hamiltonian in the $ m $-basis is then
    \begin{equation}
        \tilde{H} = U^\dagger \tilde{H} U
        \underset{\text{$ m $-basis}}{=}
        \begin{pmatrix}
            \displaystyle - \frac{3\alpha}{2} + \cos D & 0 & \sin D & 0
            \\
            0 & \displaystyle -\frac{\alpha}{2} - \cos D & 0 & \sin D
            \\
            \sin D & 0 & \displaystyle \frac{\alpha}{2} - \cos D & 0
            \\
            0 & \sin D & 0 & \displaystyle \frac{3\alpha}{2} + \cos D
        \end{pmatrix}.
    \end{equation}

    With the Hamiltonian now in a desirable basis, the system can be solved.
    The eigenenergies are most easily found via a computer-algebra system.
    Using double-primes to denote the eigenstates
    of the combined Hamiltonian, the eigenenergies are given by
    \begin{subequations}
    \begin{gather}
        \tilde{E}_{\pm 3/2''} = \mp \frac{\alpha}{2}
                                + \sqrt{\alpha^2 \mp 2 \alpha \cos D + 1}
        \\
        \tilde{E}_{\pm 1/2''} = \pm \frac{\alpha}{2}
                                - \sqrt{\alpha^2 \pm 2 \alpha \cos D + 1}.
    \end{gather}
    \end{subequations}
    It is important to note that the prime and double prime label notation
    has little to do with the actual labels used for the $ m $-basis.
    They are only used in this manner since when $ \alpha \to 0 $,
    one obtains the eigenenergies of the spin-3/2 quadrupole Hamiltonian
    and when $ \eta \to 0 $, one obtains the eigenenergies of $ I_z $.
    In fact, $ m $ is only a good quantum number when $ \eta \to 0 $.

    Next, the eigenstates are found.
    Doing so by computer can be a perilous venture
    due to some important limiting cases that will be discussed.
    Further, calculating them by hand can be more insightful.
    First denote the components of the eigenstates by $ x_i $.
    From $ \tilde{H} \ket{-3/2''} = \tilde{E}_{-3/2''} \ket{-3/2''} $,
    one finds $ x_1 = x_3 = 0 $ and the following equations:
    \begin{gather}
        \left(
            \alpha + \cos D + \sqrt{\alpha^2 + 2 \alpha \cos D + 1}
        \right) x_2
        = x_4 \sin D
        \\
        \left(
            \alpha + \cos D - \sqrt{\alpha^2 + 2 \alpha \cos D + 1}
        \right) x_4
        = -x_2 \sin D
    \end{gather}
    Understanding that $ D = 0 $ corresponds to $ \eta = 0 $,
    we keep $ \sin D $ in the numerator at all times.
    Further, when $ D = 0 $, we have
    $ \alpha + \cos D - \sqrt{\alpha^2 + 2 \alpha \cos D + 1} = 0 $,
    Hence, only the first of the two equations will be used.
    Doing so yields
    \begin{equation}
        \ket{-3/2''}
        =
        A
        \left[
            \Qty{\alpha + \cos D
                 + \sqrt{\alpha^2 + 2 \alpha \cos D + 1}} \ket{-3/2}
            + \sin D \ket{1/2}
        \right].
    \end{equation}
    The factor in front serves to remind
    that the states still need to be normalized.
    Similar reasoning leads to 
    \begin{equation}
        \ket{1/2''}
        =
        B
        \left[
            \Qty{\alpha + \cos D
                 + \sqrt{\alpha^2 + 2 \alpha \cos D + 1}} \ket{1/2}
            - \sin D \ket{-3/2}
        \right].
    \end{equation}
    For the eigenvectors corresponding to $ E_{3/2''} $ and $ E_{-1/2''} $,
    slightly more attention is required.
    Starting with $ \tilde{E}_{3/2''} $,
    one similarly obtains $ x_2 = x_4 = 0 $ and the following equations
    \begin{gather}
        \left(
            \alpha - \cos D - \sqrt{\alpha^2 - 2 \alpha \cos D + 1}
        \right) x_3
        = - x_1 \sin D
        \label{eq:-1/2eqs1}
        \\
        \left(
            \alpha - \cos D + \sqrt{\alpha^2 - 2 \alpha \cos D + 1}
        \right) x_1
        = x_3 \sin D.
        \label{eq:-1/2eqs2}
    \end{gather}
    Again, $ \sin D = 0 $ for no asymmetry.
    However, now when $ D \to 0 $,
    both equations contain singularities depending on the value of $ \alpha $.
    Keeping this in mind, we obtain
    \begin{equation}
        \ket{3/2''}
        =
        C
        \begin{cases}
            \Qty{\alpha - \cos D + \sqrt{\alpha^2 - 2 \alpha \cos D + 1}}
            \ket{-1/2}
            + \sin D \ket{3/2}
            & \alpha \geq 1
            \\
            \Qty{\alpha - \cos D - \sqrt{\alpha^2 - 2 \alpha \cos D + 1}}
            \ket{3/2}
            - \sin D \ket{-1/2}
            & \alpha \leq 1
        \end{cases}
    \end{equation}
    Similar conditions for the $ E_{-1/2''} $ case will yield
    \begin{equation}
        \ket{-1/2''}
        =
        D
        \begin{cases}
            \Qty{\alpha - \cos D + \sqrt{\alpha^2 - 2 \alpha \cos D + 1}}
            \ket{3/2}
            - \sin D \ket{-1/2}
            & \alpha \geq 1
            \\
            \Qty{\alpha - \cos D - \sqrt{\alpha^2 - 2 \alpha \cos D + 1}}
            \ket{-1/2}
            + \sin D \ket{3/2}
            & \alpha \leq 1
        \end{cases}
    \end{equation}

    The eigenstates just derived can be written more compactly
    by first observing in \Eq{H} that when $ \eta = 0 $,
    the eigenstates are just the eigenstates of $ I_z $.
    Restricting to just $ D > 0 $, it is clear from
    \Eqs{-1/2eqs1}{-1/2eqs2} that, upon normalization,
    both cases of $ \alpha \geq 1 $ and $ \alpha \leq 1 $
    will yield the same eigenstates.
    The same argument can be made for the $ -1/2'' $ case.

    In summary, the eigenenergies and normalized eigenvectors are given below.
    When $ D = 0 $, $ \ket{m''} = \ket{m} $.
    Otherwise,
    \begin{gather}
        \ket{\pm 3/2''}
        =
        A_\mp
        \left[
            \Qty{\alpha \mp \cos D
                 \mp \sqrt{(\alpha \mp \cos D)^2 + \sin^2D}} \ket{\pm 3/2}
            \mp \sin D \ket{\mp 1/2}
        \right]
        \\
        \ket{\pm 1/2''}
        =
        A_\pm
        \left[
            \Qty{\alpha \pm \cos D
                 \pm \sqrt{(\alpha \pm \cos D)^2 + \sin^2D}} \ket{\pm 1/2}
            \mp \sin D \ket{\mp 3/2}
        \right],
    \end{gather}
%    where
%    \begin{equation}
%        A_\pm
%        =
%        \left[
%            2
%            \left(
%                (\alpha \pm \cos D)^2
%                \pm (\alpha \pm \cos D) \sqrt{(\alpha \pm \cos D)^2 + \sin^2D}
%                + \sin^2D
%            \right)
%        \right]^{-1/2}
%    \end{equation}
    with eigenenergies
    \begin{gather}
        \tilde{E}_{\pm 3/2''} = \mp \frac{\alpha}{2}
                                 + \sqrt{(\alpha \mp \cos D)^2 + \sin^2 D}
        \\
        \tilde{E}_{\pm 1/2''} = \pm \frac{\alpha}{2}
                                - \sqrt{(\alpha \pm \cos D)^2 + \sin^2 D}.
    \end{gather}
    The factors $ A_\pm $ are the usual normalization factors.
    Alternatively,
    if one defines
    $ \beta \equiv \rho \alpha = \nu_\mathrm{L}/\nu_\mathrm{Q} $,
    the solutions may be written in terms of $ \beta $ and $ \eta $,
    which may be more natural:
    \begin{gather}
        \ket{\pm 3/2''}
        =
        A_\mp
        \left[
            \Qty{\beta \mp 1
                 \mp \sqrt{(\beta \mp 1)^2 + \frac{\eta^2}{3}}} \ket{\pm 3/2}
            \mp \frac{\eta}{\sqrt{3}} \ket{\mp 1/2}
        \right]
        \\
        \ket{\pm 1/2''}
        =
        A_\pm
        \left[
            \Qty{\beta \pm 1
                 \pm \sqrt{(\beta \pm 1)^2 + \frac{\eta^2}{3}}} \ket{\pm 1/2}
            \mp \frac{\eta}{\sqrt{3}} \ket{\mp 3/2}
        \right],
    \end{gather}
%    where
%    \begin{equation}
%        A_\pm
%        =
%        \left[
%            \left(
%                \beta \pm 1 \pm \sqrt{(\beta \pm 1)^2 + \frac{\eta^2}{3}}
%            \right)^2
%            + \frac{\eta^2}{3}
%        \right]^{-1/2},
%    \end{equation}
    with eigenenergies
    \begin{gather}
        \frac{E_{\pm 3/2''}}{h \nu_\mathrm{Q}}
        = \mp \frac{\beta}{2} + \sqrt{(\beta \mp 1)^2 + \frac{\eta^2}{3}}
        \\
        \frac{E_{\pm 1/2''}}{h \nu_\mathrm{Q}}
        = \pm \frac{\beta}{2} - \sqrt{(\beta \pm 1)^2 + \frac{\eta^2}{3}}.
    \end{gather}
    The energies are visualized in \Fig{energy}
    for various values of $ \beta $.

    \begin{figure}[htbp]
        \centering%
        \includegraphics[scale=1.2]{energy.jpg}
        \caption{%
            Eigenenergies of the combined Hamiltonian for various values
            of $ \beta = \nu_\mathrm{L}/\nu_\mathrm{Q} $
            plotted as a function of all possible values
            of the EFG asymmetry parameter $ \eta $.
            The degeneracy of the pure spin 3/2 ZFNMR system can be seen
            in the $ \beta = 0 $ plot.
            The introduction of a magnetic field produces the Zeeman effect,
            removing the degeneracy, as seen in the other three plots.%
        }
        \label{fig:energy}
    \end{figure}

%    Alternatively,
%    if one defines $ k_\pm = \sqrt{3} (\nu_\mathrm{L}/\nu_\mathrm{Q} \pm 1) $,
%    the solutions may be written in terms of $ k $ and $ \eta $,
%    which may be more natural:
%    \begin{gather}
%        \ket{\pm 3/2''}
%        =
%        A_\pm
%        \left[
%            \Qty{k_\pm \pm \sqrt{k_\pm^2 + \eta^2}} \ket{\pm 3/2}
%            \pm \eta \ket{\mp 1/2}
%        \right]
%        \\
%        \ket{\pm 1/2''}
%        =
%        A_\mp
%        \left[
%            \Qty{k_\mp \mp \sqrt{k_\mp^2 + \eta^2}} \ket{\pm 1/2}
%            \pm \eta \ket{\mp 3/2}
%        \right],
%    \end{gather}
%    where
%    \begin{equation}
%        A_\pm
%        =
%        \left[
%            \left(
%                k_\pm \pm \sqrt{k_\pm^2 + \eta^2}
%            \right)^2
%            + \eta^2
%        \right]^{-1/2},
%    \end{equation}
%    with eigenenergies
%    \begin{gather}
%        \frac{E_{\pm 3/2''}}{h \nu_\mathrm{Q}}
%        = \pm \frac{k_\pm}{2 \sqrt{3}} - \frac{1}{2} + \sqrt{k_\pm^2 + \eta^2}
%        \\
%        \frac{E_{\pm 1/2''}}{h \nu_\mathrm{Q}}
%        = \mp \frac{k_\pm}{2 \sqrt{3}} - \frac{1}{2} - \sqrt{k_\mp^2 + \eta^2}.
%    \end{gather}

    \subsection{Matrix Elements and Transition Frequencies}
    Next, the matrix elements $ \braket{m''|\Vb{I}|n''} $ are investigated
    in order to facilitate the derivation of the transition frequencies.

    Some common experiments in which these matrix elements would be
    the most useful is in NMR and ZFNMR.
    The direct observable in these experiments is the magnetic moment,
    which is directly proportional to $ \braket{\Vb{I}} $.
    With the matrix elements $ \braket{m''|\Vb{I}|n''} $,
    one can calculate the transition rates according to Fermi's Golden Rule,
    or to higher order
    (which is necessarily the case when studying the perturbations
    associated with the excitation pulses in NMR and ZFNMR).
    Further, one can find any arbitrary transition frequency
    by taking a combination of any two energy levels,
    but because the $ m'' $ states are mixtures of $ m $ states,
    the usual angular momentum selection rules do not apply,
    and so one cannot immediately know
    which transitions are allowed or forbidden.

    We begin by noting the usual relations of raising and lowering operators:
    $ 2 I_x = I_+ + I_- $ and $ 2 \I I_y = I_+ - I_- $.
    For convenience, let $ C_i $ denote the probability amplitudes of the kets.
    We have
    \begin{align}
        2 I_x\ket{\pm 3/2''}
        \notag
        &=
        (I_+ + I_-)
        \Qty{C_1 \ket{\pm 3/2} + C_ 2 \ket{\mp 1/2}}
        \notag
        \\
        &=
        C_1 \ket{\pm 1/2} + C_2 \ket{\pm 1/2} \pm C_2 \ket{\mp 3/2}.
    \end{align}
    Clearly, the only states that will make
    $ \braket{m''|2 I_x|\pm 3/2} \neq 0 $
    are $ m'' = 1/2, -3/2'' $ for the $ + $ case
    and $ m'' = 3/2'', -1/2'' $ in the $ - $ case.
    Of course, this result applies to $ I_y $ as well.
    Since $ I_z $ does not alter the states,
    only $ m'' = \mp 1/2'' $ will be allowed.
    The same analysis can be done for $ \ket{\pm 1/2''} $.
    The conclusion can be summarized by the following modified
    selection rules:
    \begin{subequations}
        \begin{align}
            \Abs{\Delta m''} &= 1, 3;\quad I_x, I_y
            \\
            \Abs{\Delta m''} &= 0, 2; \quad I_z.
        \end{align}
        \label{eq:selection rules}
    \end{subequations}

    Based on the modified selection rules
    in \Eq{selection rules},
    one finds that there are a total of 6 allowed transitions.
    For completeness, the frequencies are given below
    and visualized in \Fig{freqs}
    \begin{subequations}
        \begin{align}
            -3/2'' \leftrightarrow -1/2'':&\quad
            \beta + \sqrt{(\beta - 1)^2 + \frac{\eta^2}{3}}
            + \sqrt{(\beta + 1)^2 + \frac{\eta^2}{3}}
            \\
            +3/2'' \leftrightarrow -3/2'':&\quad
            \beta - \sqrt{(\beta - 1)^2 + \frac{\eta^2}{3}}
            + \sqrt{(\beta + 1)^2 + \frac{\eta^2}{3}}
            \\
            +1/2'' \leftrightarrow -1/2'':&\quad
            - \beta - \sqrt{(\beta - 1)^2 + \frac{\eta^2}{3}}
            + \sqrt{(\beta + 1)^2 + \frac{\eta^2}{3}}
            \\
            +3/2'' \leftrightarrow +1/2'':&\quad
            - \beta + \sqrt{(\beta - 1)^2 + \frac{\eta^2}{3}}
            + \sqrt{(\beta + 1)^2 + \frac{\eta^2}{3}}
            \\
            -3/2'' \leftrightarrow +1/2'':&\quad
            2 \sqrt{(\beta + 1)^2 + \frac{\eta^2}{3}}
            \\
            +3/2'' \leftrightarrow -1/2'':&\quad
            2 \sqrt{(\beta - 1)^2 + \frac{\eta^2}{3}}.
        \end{align}
    \end{subequations}
    However, not all of these transitions are equally likely
    and so the transition rates will vary.
    In addition, under an excitation pulse which perturbs the system,
    detecting those weaker transitions can further complicate the matter
    making some transitions very difficult to observe.
    In any circumstance, a first step to understand
    how weak those transitions may be is to calculate the matrix
    elements of a desirable operator.
    For example, in the case of iron-based superconductors,
    one finds that the internal magnetic field orients itself
    in the $ z $ direction of the EFG principle axes frame.
    In such a system, performing a ZFNMR experiment involves
    applying excitation radio frequency pulses perpendicular
    to the internal field.
    Hence, one may be interested in the transition rates associated
    with the $ I_x $ or $ I_y $ operator.
    By determining the matrix elements $ \braket{m''|I_i|n''} $
    for $ i = x, y $, one will be able to calculate the transition rates
    via Fermi's Golden Rule
    or use higher-order time-dependent perturbation theory.
    Deriving the matrix elements analytically can be tedious.
    Hence, they are calculated numerically
    and visualized in \Fig{magmom}.
    Based off of this calculation,
    the three most easily observable frequencies are
    \begin{align}
        +3/2'' \leftrightarrow -3/2'':&\quad
        \beta - \sqrt{(\beta - 1)^2 + \frac{\eta^2}{3}}
        + \sqrt{(\beta + 1)^2 + \frac{\eta^2}{3}}
        \\
        +3/2'' \leftrightarrow +1/2'':&\quad
        - \beta + \sqrt{(\beta - 1)^2 + \frac{\eta^2}{3}}
        + \sqrt{(\beta + 1)^2 + \frac{\eta^2}{3}}
        \\
        +1/2'' \leftrightarrow -1/2'':&\quad
        - \beta - \sqrt{(\beta - 1)^2 + \frac{\eta^2}{3}}
        + \sqrt{(\beta + 1)^2 + \frac{\eta^2}{3}};
    \end{align}
    and the fourth weaker transition is
    \begin{align}
        -3/2'' \leftrightarrow -1/2'':&\quad
        \beta + \sqrt{(\beta - 1)^2 + \frac{\eta^2}{3}}
        + \sqrt{(\beta + 1)^2 + \frac{\eta^2}{3}}.
    \end{align}

    \begin{figure}[htbp]
        \centering%
        \includegraphics[scale=1.2]{freqs.jpg}
        \caption{%
            All 6 possible transition frequencies of the combined Hamiltonian
            for various values of $ \beta = \nu_\mathrm{L}/\nu_\mathrm{Q} $
            plotted as a function of all possible values
            of the EFG asymmetry parameter $ \eta $.
            Although all transitions displayed are possible,
            some are weaker.%
        }
        \label{fig:freqs}
    \end{figure}

    \begin{figure}[htbp]
        \centering%
        \includegraphics[scale=1.2]{magmom.jpg}
        \caption{%
            Visualization of the magnetic moment,
            which is proportional to the matrix elements
            $ \braket{n''|I_i|m''} $ for $ i = x, y $.
            A value of $ \beta = 1.33 $,
            corresponding to comparable Zeeman and quadrupole effects,
            is chosen.
            Comparing this to \Fig{freqs},
            it is clear that
            some frequencies will be more difficult to observe.%
        }
        \label{fig:magmom}
    \end{figure}

    \section{Conclusions and Future Work}
    The eigenenergies, eigenstates, and transition frequencies
    of the combined Zeeman and quadrupolar Hamiltonian
    have been derived analytically and exactly in the particular geometry
    of a magnetic field oriented in the $ z $ direction
    of the EFG principle axes.
    The results are useful for real systems found in condensed-matter,
    namely iron-based superconductors.
    Although the results are applicable to real system,
    generalizing these results to arbitrary magnetic field orientation
    will prove to be more applicable.
    In addition, the results can be generalized further
    by assuming an arbitrary coordinate system,
    as opposed to the EFG principle axes frame.


    \newpage

%    \bibliographystyle{plain}
    \bibliography{bib}

    \newpage

    \appendix
    \section{Representation of $ I_z $ in the Quadrupolar Basis}
    \label{app:IzQ}
    The following are the matrix elements of $ I_z $
    in the quadrupolar basis, given in \Eq{qbasis}.
    A factor of 1/2 is assumed in front of all terms.
    For clarity, define $ \delta = D/2 $.
    \begin{align}
        \bra{+3/2'}I_z\ket{+3/2'} &= 3\cos^2\delta - \sin^2\delta
        \\
        \bra{+1/2'}I_z\ket{+3/2'} &= 0
        \\
        \bra{-1/2'}I_z\ket{+3/2'} &= -3\cos\delta\sin\delta - \cos\delta\sin\delta
        \\
        \bra{-3/2'}I_z\ket{+3/2'} &= 0
        \\
        \notag
        \\
        \bra{+3/2'}I_z\ket{+1/2'} &= 0
        \\
        \bra{+1/2'}I_z\ket{+1/2'} &= \cos^2\delta - 3\sin^2\delta
        \\
        \bra{-1/2'}I_z\ket{+1/2'} &= 0
        \\
        \bra{-3/2'}I_z\ket{+1/2'} &= 3 \cos\delta\sin\delta + \cos\delta\sin\delta
        \\
        \notag
        \\
        \bra{+3/2'}I_z\ket{-1/2'} &= -\cos\delta\sin\delta - 3\cos\delta\sin\delta
        \\
        \bra{+1/2'}I_z\ket{-1/2'} &= 0
        \\
        \bra{-1/2'}I_z\ket{-1/2'} &= -\cos^2\delta + 3\sin^2\delta
        \\
        \bra{-3/2'}I_z\ket{-1/2'} &= 0
        \\
        \notag
        \\
        \bra{+3/2'}I_z\ket{-3/2'} &= 0
        \\
        \bra{+1/2'}I_z\ket{-3/2'} &= 3\cos\delta\sin\delta + \cos\delta\sin\delta
        \\
        \bra{-1/2'}I_z\ket{-3/2'} &= 0
        \\
        \bra{-3/2'}I_z\ket{-3/2'} &= -3\cos^2\delta + \sin^2\delta
    \end{align}
    Using half-angle formulas, one obtains
    \begin{equation}
        I_z
        \underset{\text{q-basis}}{=}
        \begin{pmatrix}
            \cos D + 1/2 & 0 & -\sin D & 0
            \\
            0 & \cos D - 1/2 & 0 & \sin D
            \\
            -\sin D & 0 & 1/2 - \cos D & 0
            \\
            0 & \sin D & 0 & -1/2 - \cos D
        \end{pmatrix}.
    \end{equation}

\end{document}
