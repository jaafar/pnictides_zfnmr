\documentclass[12pt]{revtex4-2}
\usepackage{booktabs}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{mhchem}
\usepackage{microtype}
\usepackage{mlmodern}
\usepackage{siunitx}
\usepackage{subcaption}
\usepackage{xparse}


\NewDocumentCommand{\Eq}{sm}{%
    \IfBooleanTF{#1}{%
        Equation~(\ref{eq:#2})%
    }{%
        Eq.~(\ref{eq:#2})%
    }%
}

\NewDocumentCommand{\Eqs}{smm}{%
    \IfBooleanTF{#1}{%
        Equations~(\ref{eq:#2}-\ref{eq:#3})%
    }{%
        Eqs.~(\ref{eq:#2}-\ref{eq:#3})%
    }%
}

\NewDocumentCommand{\Fig}{sm}{%
    \IfBooleanTF{#1}{%
        Figure~(\ref{fig:#2})%
    }{%
        Fig.~(\ref{fig:#2})%
    }%
}

\NewDocumentCommand{\Sec}{m}{%
    Section~\ref{sec:#1}%
}

\NewDocumentCommand{\App}{m}{%
    Appendix~\ref{app:#1}%
}

\NewDocumentCommand{\Vb}{sm}{%
    % Unit vector [star for unit vectors]
    \IfBooleanTF{#1}{%
%        \symbf{\hat{#2}}%
        \mathbf{\hat{#2}}%
    }{%
%        \symbf{#2}%
        \mathbf{#2}%
    }%
}

\NewDocumentCommand{\Qty}{m}{%
    \left(#1\right)%
}


\begin{document}
    \title{Mixing Results Summary}
    \date{\today}
    \maketitle

    Let $ V_\mathrm{M} $ and $ V_\mathrm{N} $ be the EFG tensors from DFT
    calculated with spin-polarization on and off, respectively.
    Also, let $ V_\mathrm{exp} $ be the EFGs obtained from experiment.
    We propose, to first approximation, the following model
    \begin{equation}
        k V_\mathrm{M} + (k - 1) V_\mathrm{N} = V_\mathrm{exp}.
    \end{equation}
    The results for \ce{LaFeAsO} and \ce{BaFe2As2} are summarized below
    in the tables below.
    The $ V_{ii} $ are the EFG principle axes components, written
    with respect to the crystallographic frame
    and in units of $ 10^{21} \si{V m^{-2}} $.
    The rows with a $ k $ value show the actual mixing results
    and those with a $ T $ value show the experimental EFG at that temperature.

    The following is a derivation of the formula for the internal field
    from dipolar irons.
    The geometry is described in \Fig{geometry}.
    Let $ \Vb{r}_i $ be the vector from $ \ce{Fe}_i $ to \ce{As}.
    Then in Cartesian coordinates,
    \begin{align}
        \Vb{r}_1 &= -\Qty{+\frac{a}{4}, +\frac{b}{4}, -zc}
        \\
        \Vb{r}_2 &= -\Qty{-\frac{a}{4}, +\frac{b}{4}, -zc}
        \\
        \Vb{r}_3 &= -\Qty{-\frac{a}{4}, -\frac{b}{4}, -zc}
        \\
        \Vb{r}_4 &= -\Qty{+\frac{a}{4}, -\frac{b}{4}, -zc}.
    \end{align}
    The magnetic moments corresponding to $ \ce{Fe}_i $ are given by
    \begin{align}
        \Vb{m}_1 &= \Qty{-m, 0, 0}
        \\
        \Vb{m}_2 &= \Qty{+m, 0, 0}
        \\
        \Vb{m}_3 &= \Qty{+m, 0, 0}
        \\
        \Vb{m}_4 &= \Qty{-m, 0, 0}.
    \end{align}
    Since the irons are all the same distance away from \ce{As},
    let $ r \equiv | \Vb{r}_i | = (a^2 + b^2 + (4 z c)^2)^{1/2}/4 $.
    In SI units, the internal field is then given by
    \begin{align}
        \Vb{B}_\mathrm{int}(z)
        &=
        \sum_{i=1}^4
        \frac{\mu_0}{4 \pi}
        \left(
            \frac{3 \Vb{r}_i (\Vb{m}_i \cdot \Vb{r}_i)}{r_i^5}
            -
            \frac{\Vb{m}_i}{r_i^3}
        \right)
        \\
        &=
        \sum_{i=1}^4
        \frac{\mu_0}{4 \pi}
        \left(
            \frac{3 m a}{4 r^5} \Vb{r}_i
            -
            \frac{\Vb{m}_i}{r^3}
        \right)
        \\
        &=
        \frac{\mu_0}{4 \pi}
        \frac{3 m a}{r^5} z c \Vb*{c}
        \\
        &=
        \frac{3 \mu_0}{4 \pi}
        \frac{m a c}{r^5} z \Vb*{c}.
    \end{align}
    For \ce{LaFeAsO} $ z = 0.1513 $
    and for \ce{BaFe2As2} $ z = 0.10375 $.

    \begin{figure}
        \centering%
        \includegraphics[scale=0.15]{cell.png}
        \caption{%
            The origin is centered on \ce{As},
            with the irons a vertical distance $ z c $
            below the \ce{As} plane.
            It is assumed that the irons are antiferromagnetic along $ a $
            and ferromagnetic along $ b $.%
        }
        \label{fig:geometry}
    \end{figure}

    \begin{table}[htbp]
        \centering
        \caption{LaFeAsO}
        \begin{subtable}[t]{0.6\linewidth}
            \centering
            \caption{EFG}
            \begin{tabular}{lS[table-format=+1.4]
                             S[table-format=+1.4]
                             S[table-format=+1.4]
                             S[table-format=+1.4]}
                \toprule
                & \multicolumn{1}{c}{$ V_{aa} $}
                & $ V_{bb} $ & $ V_{cc} $
                \\ \midrule
                $ V_\mathrm{M} $
                & -0.7877
                & 1.662
                & -0.8743
                \\
                $ V_\mathrm{N} $
                    & 1.399
                    & 1.404
                    & -2.802
                \\ \midrule
                $ T = \SI{4.2}{K} $ \cite{Moroni2017}
                    & -0.792
                    & -1.54
                    & 2.33
                \\
                $ k = 0.28 $
                    & 0.792
                    & 1.48
                    & -2.27
                \\ \midrule
                $ T = \SI{145}{K} $ \cite{Fu2012}
                    & -1.02
                    & -1.36
                    & 2.39
                \\
                $ k = 0.17 $
                    & 1.03
                    & 1.45
                    & -2.47
                \\ \bottomrule
            \end{tabular}
        \end{subtable}%
        \begin{subtable}[t]{0.4\linewidth}
            \centering
            \caption{Fe Magnetic Moment}
            \begin{tabular}{lccc}
                \toprule
                Method & $ T $ (\si{K}) & $ m $ (\si{\mu_B}) & $ B_\mathrm{int} $ (\si{T})
                \\ \midrule
                Neutrons \cite{Li2010} & 9.5 & 0.8 & 0.2
                \\
                DFT & - & 2.1 & 0.5
                \\
                NMR \cite{Fu2012} & 5 & 6.2 & 1.6
                \\ \bottomrule
            \end{tabular}
        \end{subtable}
    \end{table}

    \begin{table}[htbp]
        \centering
        \caption{\ce{BaFe2As2}}
        \begin{subtable}[t]{0.6\linewidth}
            \centering
            \caption{EFG}
            \begin{tabular}{llS[table-format=+1.4]
                             S[table-format=+1.4]
                             S[table-format=+1.4]
                             S[table-format=+1.4]}
                \toprule
                \% Co & & \multicolumn{1}{c}{$ V_{aa} $}
                & $ V_{bb} $ & $ V_{cc} $
                \\ \midrule
            0.0 &
            $ V_\mathrm{M} $
                & -1.701
                & 1.351
                & 0.3506
            \\
            &
            $ V_\mathrm{N} $
                & 0.3519
                & 0.5582
                & -0.9101
                \\ \midrule
            0.02 &
            $ V_\mathrm{M} $
                & -1.616
                & 1.312
                & 0.3041
            \\
            &
            $ V_\mathrm{N} $
                & 0.3727
                & 0.5620
                & -0.9346
                \\ \midrule
            0.08 &
            $ V_\mathrm{M} $
                & -1.534
                & 1.338
                & 0.1965
            \\
            &
            $ V_\mathrm{N} $
                & 0.3637
                & 0.5546
                & -0.9183
            \\ \midrule
            $ T = \SI{10}{K} $ \cite{Kitagawa2008}
                & 0.0541
                & -0.639
                & 0.585
            \\
            $ k = 0.198 $
                & -0.055
                & 0.715
                & -0.66
            \\ \midrule
            $ T = \SI{135}{K} $ \cite{Toyoda2018}
                & -0.051
                & -0.529
                & 0.580
            \\
            $ k = 0.196 $
                & -0.051
                & 0.714
                & -0.663
            \\ \bottomrule
            \end{tabular}
        \end{subtable}%
        \begin{subtable}[t]{0.4\linewidth}
            \centering
            \caption{Fe Magnetic Moment}
            \begin{tabular}{lccc}
                \toprule
                Method & $ T $ (\si{K}) & $ m $ (\si{\mu_B}) & $ B_\mathrm{int} $ (\si{T})
                \\ \midrule
                Neutrons \cite{Huang2008} & 5 & 0.87 & 0.23
                \\
                DFT & - & 2.0 & 0.5
                \\
                NMR \cite{Kitagawa2008} & 10 & 5.6 & 1.5
                \\ \bottomrule
            \end{tabular}
        \end{subtable}
    \end{table}

    \newpage

    \bibliography{bib}

\end{document}
