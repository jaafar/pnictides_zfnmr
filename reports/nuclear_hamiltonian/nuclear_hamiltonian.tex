\documentclass[12pt]{article}
\usepackage{amsmath}
\usepackage{braket}
\usepackage{fontspec}
\usepackage[margin=1in]{geometry}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{mathtools}
\usepackage{microtype}
\usepackage{siunitx}
\usepackage{unicode-math}
\usepackage{xparse}

% Fonts
\setmainfont{lmroman10-regular.otf}[
    Path=../fonts/,
    BoldFont=lmroman10-bold.otf,
    ItalicFont=lmroman10-italic.otf,
    BoldItalicFont=lmroman10-bolditalic.otf]
\setmathfont[Path=../fonts/]{latinmodern-math.otf}
\setsansfont{lmsans10-regular.otf}[
    Path=../fonts/,
    BoldFont=lmsans10-bold.otf,
    ItalicFont=lmsans10-oblique.otf,
    BoldItalicFont=lmsans10-boldoblique.otf]
\setmonofont{lmmonoprop10-regular.otf}[
    Path=../fonts/,
    BoldFont=lmmonoproplt10-bold.otf,
    ItalicFont=lmmonoprop10-oblique.otf,
    BoldItalicFont=lmmonoproplt10-boldoblique.otf]

\NewDocumentCommand{\Fig}{m}{%
    Figure~\ref{fig:#1}%
}

\NewDocumentCommand{\Eq}{m}{%
    Eq.~\ref{eq:#1}%
}

\NewDocumentCommand{\wi}{}{%
    \omega_\mathrm{i}%
}

\NewDocumentCommand{\Qty}{m}{%
    \left(#1\right)%
}

\NewDocumentCommand{\Qtyb}{m}{%
    \left[#1\right]%
}

\DeclareMathOperator{\sign}{sgn}


\begin{document}

    {%
        \centering%
        \large%
        \bf%
        LaFeAsO

        Last updated: \today

    }


    \vspace{\baselineskip}

    \section{Hamiltonian}

    We write the nuclear spin Hamiltonian in the basis
    of the pure quadrupolar Hamiltonian.
    From Suits~\cite{Suits2006}, this is given by
    \begin{align}
        \ket{\pm 3/2'} &= \cos\delta \ket{\pm 3/2} + \sin\delta \ket{\mp 1/2}
        \\
        \ket{\pm 1/2'} &= \cos\delta \ket{\pm 1/2} - \sin\delta \ket{\mp 3/2},
    \end{align}
    where the un-primed eigenstates are pure Zeeman states,
    $ \sin\delta = \sqrt{(\rho - 1)/2\rho} $,
    and $ \rho = \sqrt{1 + \eta^2/3} $.
    Note that since $ 0 \leq \eta \leq 1 $,
    then $ 0 \leq \sin \delta \leq \sqrt{2 - \sqrt{3}}/2 $
    or $ 0 \leq \delta \leq \pi/12 $.

    Subject to a magnetic field $ B_\mathrm{i} \hat{z} $,
    the Zeeman Hamiltonian is $ H_\mathrm{Z}/\hbar = \wi I_z $,
    where $ \wi = B_\mathrm{i} \gamma $.
    So, we may write $ H_\mathrm{Z} $ in the representation
    $ \bra{m'} H_\mathrm{Z}/\hbar \ket{n'} $.
    To aid in accountability and later verification,
    each matrix element is written out in full in Appendix~\ref{sec:appendix}.
    So, in matrix form, we have
    \begin{align}
        H_\mathrm{Z}
        &\coloneqq
        \frac{\hbar \wi}{2}
        \begin{pmatrix}
            3\cos^2\delta - \sin^2\delta
            & 0
            & -4\cos\delta \sin\delta
            & 0
            \\
            0
            & \cos^2\delta - 3\sin^2\delta
            & 0
            & 4\cos\delta \sin\delta
            \\
            -4\cos\delta \sin\delta
            & 0
            & -\cos^2\delta + 3\sin^2\delta
            & 0
            \\
            0
            & 4\cos\delta \sin\delta
            & 0
            & -3\cos^2\delta + \sin^2\delta
            \\
        \end{pmatrix}
        \\
        &=
        \frac{\hbar \omega_i}{2}
        \begin{pmatrix}
            2\cos(2\delta) + 1
            & 0
            & -2\sin(2\delta)
            & 0
            \\
            0
            & 2\cos(2\delta) - 1
            & 0
            & 2\sin(2\delta)
            \\
            -2\sin(2\delta)
            & 0
            & 1 - 2\cos(2\delta)
            & 0
            \\
            0
            & 2\sin(2\delta)
            & 0
            & -1 - 2\cos(2\delta)
            \\
        \end{pmatrix}
        \\
        &=
        \frac{\hbar \omega_i}{2}
        \begin{pmatrix}
            2\cos D + 1
            & 0
            & -2\sin D
            & 0
            \\
            0
            & 2\cos D - 1
            & 0
            & 2\sin D
            \\
            -2\sin D
            & 0
            & 1 - 2\cos D
            & 0
            \\
            0
            & 2\sin D
            & 0
            & -1 - 2\cos D
        \end{pmatrix},
    \end{align}
    where $ D \equiv 2\delta $.
    The quadrupolar Hamiltonian (in the EFG's principal axes frame)
    is given by
    \begin{equation}
        H_Q = \frac{1}{3} \hbar\omega_Q
              \Qty{3 I_z^2 - I^2 + \eta(I_x^2 - I_y^2)}
    \end{equation}
    and has eigenenergies
    $ E_{\pm 3/2'} = \hbar \omega_Q \rho $
    and $ E_{\pm 1/2'} = - \hbar \omega_Q \rho $,
    where $ \omega_Q = 3 e V_{zz} Q/(4I(2I - 1)\hbar) $.
    So, in the quadrupolar basis, we have
    $ \bra{m'} H_Q \ket{n'} = E_{n'} \braket{m'}{n'} = E_{n'} \delta_{m'n'} $.
    In matrix form,
    \begin{equation}
        H_Q \coloneqq
        \hbar \omega_Q \rho
        \begin{pmatrix}
            1 & 0 & 0 & 0
            \\
            0 & -1 & 0 & 0
            \\
            0 & 0 & -1 & 0
            \\
            0 & 0 & 0 & 1
        \end{pmatrix}
    \end{equation}
    We can fully characterize this system with
    $ \alpha \equiv \omega_Q \rho/\omega_i $ and $ D $.
    The total Hamiltonian is then
    \begin{align}
        \frac{H}{\hbar \omega_i}
        &=
        \alpha
        \begin{pmatrix}
            1 & 0 & 0 & 0
            \\
            0 & -1 & 0 & 0
            \\
            0 & 0 & -1 & 0
            \\
            0 & 0 & 0 & 1
        \end{pmatrix}
        \notag
        \\
        &
        \phantom{{}=\quad}
        +
        \begin{pmatrix}
            \cos D + 1/2
            & 0
            & -\sin D
            & 0
            \\
            0
            & \cos D - 1/2
            & 0
            & \sin D
            \\
            -\sin D
            & 0
            & 1/2 - \cos D
            & 0
            \\
            0
            & \sin D
            & 0
            & -1/2 - \cos D
        \end{pmatrix}
        \\
        &=
        \begin{pmatrix}
            a_+ + 1/2 & 0 & -\sin D & 0
            \\
            0
            & - a_- - 1/2 & 0 & \sin D
            \\
            -\sin D & 0 & - a_+ + 1/2 & 0
            \\
            0 & \sin D & 0 & a_- - 1/2
        \end{pmatrix},
        \label{eq:H}
    \end{align}
    where $ a_\pm \equiv \alpha \pm \cos D $.
    The eigenvalues are
    \begin{align}
        E_{3/2'', -1/2''} = \pm \sqrt{a_+^2 + \sin^2 D} + \frac{1}{2}
        \\
        E_{-3/2'', 1/2''} = \pm a_-\sqrt{1 + \Qty{\frac{\sin D}{a_-}}^2} - \frac{1}{2}
    \end{align}
    and the eigenvectors in the primed basis are
    \begingroup
    \addtolength{\jot}{1em}
    \begin{gather}
        \ket{+3/2''} =
        \begin{pmatrix}
            1 & 0
            & \displaystyle \frac{-\sin D}{a_+ + \sqrt{a_+^2 + \sin^2 D}} & 0
        \end{pmatrix}
        \\
        \ket{-1/2''} =
        \begin{pmatrix}
            \displaystyle \frac{\sin D}{a_+ + \sqrt{a_+^2 + \sin^2 D}}
            & 0 & 1 & 0
        \end{pmatrix}
        \\
        \ket{-3/2''} =
        \begin{pmatrix}
            0
            & \displaystyle \frac{\sin D}{a_- + a_-\sqrt{1 + (\sin D/a_-)^2}}
            & 0 & 1
        \end{pmatrix}
        \\
        \ket{+1/2''} =
        \begin{pmatrix}
            0 & 1 & 0
            & \displaystyle \frac{-\sin D}{a_- + a_- \sqrt{1 + \Qty{\sin D/a_-}^2}}
        \end{pmatrix}
    \end{gather}
    \endgroup
    and in the $ m $-basis
    \begingroup
    \addtolength{\jot}{1em}
    \begin{gather}
        \ket{+3/2''} =
        \begin{pmatrix}
            \displaystyle
            \frac{\sin D \sin\delta}{a_+ + \sqrt{a_+^2 + \sin^2 D}} + \cos \delta
            & 0 &
            \displaystyle
            \frac{-\sin D \cos\delta}{a_+ + \sqrt{a_+^2 + \sin^2 D}} + \sin \delta
            & 0
        \end{pmatrix}
        \\
        \ket{-1/2''} =
        \begin{pmatrix}
            \displaystyle
            \frac{\sin D \cos\delta}{a_+ + \sqrt{a_+^2 + \sin^2 D}} - \sin \delta
            & 0 &
            \displaystyle
            \frac{\sin D \sin\delta}{a_+ + \sqrt{a_+^2 + \sin^2 D}} + \cos \delta
            & 0
        \end{pmatrix}
        \\
        \ket{-3/2''} =
        \begin{pmatrix}
            0 &
            \displaystyle
            \frac{\sin D \cos\delta}{a_- + a_-\sqrt{1 + \sin^2 D/a_-^2}} + \sin \delta
            & 0 &
            \displaystyle
            \frac{-\sin D \sin\delta}{a_- + a_-\sqrt{1 + \sin^2 D/a_-^2}} + \cos \delta
        \end{pmatrix}
        \\
        \ket{+1/2''} =
        \begin{pmatrix}
            0 &
            \displaystyle
            \frac{-\sin D \sin\delta}{a_- + a_-\sqrt{1 + \sin^2 D/a_-^2}} + \cos \delta
            & 0 &
            \displaystyle
            \frac{-\sin D \cos\delta}{a_- + a_-\sqrt{1 + \sin^2 D/a_-^2}} - \sin \delta
        \end{pmatrix}
    \end{gather}
    \endgroup
    Note that none of these vectors are normalized.
    We keep the factor of $ a_- $ outside of the square root
    in the above expressions since it changes sign at $ \alpha = \cos D $,
    resulting in a different set of eigenvalues and eigenstates.

    We greatly simplify these expressions by rewriting
    the eigenvector components in terms of the eigenvalues.
    First define the following quantities:
    \begin{equation}
        g_\pm = \alpha
                + \sign(\alpha \pm \cos D)
                  \sqrt{\Qty{\alpha \pm \cos D}^2 + \sin^2 D},
    \end{equation}
    \begin{equation}
        A_\pm = \frac{\sign(g_\pm \pm \cos D)}
                     {\sqrt{(g_\pm \pm \cos D)^2 + \sin^2 D}}.
    \end{equation}
    Then, the eigenvalues become
    \begin{align}
        E_{\pm 3/2''} &= g_\pm - \alpha \pm \frac{1}{2},
        \\
        E_{\pm 1/2''} &= -\Qty{g_\mp - \alpha \pm \frac{1}{2}}.
    \end{align}
    And the normalized eigenvectors in the $ m $-basis are
    \begin{align}
        \ket{\pm 3/2''}
        &= A_{\pm}
           \Qtyb{\Qty{g_\pm \pm 1} \cos\delta \ket{\pm 3/2}
                 + \Qty{g_\pm \mp 1} \sin\delta \ket{\mp 1/2}}
        \\
        \ket{\pm 1/2''}
        &= A_\mp
           \Qtyb{\Qty{g_\mp \mp 1} \cos\delta \ket{\pm 1/2}
                - \Qty{g_\mp \pm 1} \sin\delta \ket{\mp 3/2}}.
    \end{align}

    or

    The eigenvalues are
    \begin{align}
        E_{\pm 3/2''} &= \sign(\alpha \pm \cos D)
                         \sqrt{(\alpha \pm \cos D)^2 + \sin^2 D}
                         \pm \frac{1}{2},
        \\
        E_{\pm 1/2''} &= - \sign(\alpha \mp \cos D)
                         \sqrt{(\alpha \mp \cos D)^2 + \sin^2 D}
                         \mp \frac{1}{2}.
    \end{align}


%    \begin{align}
%        \ket{\pm 3/2''}
%        &= G_{\pm \pm} \cos\delta \ket{\pm 3/2}
%           + G_{\pm \mp} \sin\delta \ket{\mp 1/2}
%        \\
%        \ket{\pm 1/2''}
%        &= G_{\mp \mp} \cos\delta \ket{\pm 1/2}
%           - G_{\mp \pm} \sin\delta \ket{\mp 3/2}
%    \end{align}

%    First define 4 new quantities $ g_{j k} $
%    \begin{align}
%        g_{j k} = \qty(\alpha + j \cos D)
%                  \sqrt{1 + \qty(\frac{\sin D}
%                                     {\alpha + j \cos D})^2}
%                 + \alpha + k,
%    \end{align}
%    where $ j $ and $ k $ are either $ +1 $ or $ -1 $.
%    In this notation, the eigenvectors in the $ m $-basis are
%    \begingroup
%    \addtolength{\jot}{1em}
%    \begin{gather}
%        \ket{3/2''},
%        \ket{1/2''},
%        \ket{-1/2''},
%        \ket{-3/2''}
%        =
%        \begin{pmatrix}
%            \displaystyle
%            %g_{++} \cos\delta & 0 & g_{+-} \sin\delta & 0
%            g_{++} \cos\delta \\ 0 \\ g_{+-} \sin\delta \\ 0
%        \end{pmatrix}
%        \begin{pmatrix}
%            \displaystyle
%%            0 & g_{--} \cos\delta & 0 & -g_{-+} \sin\delta
%            0 \\ g_{--} \cos\delta \\ 0 \\ -g_{-+} \sin\delta
%        \end{pmatrix}
%        \begin{pmatrix}
%            \displaystyle
%            -g_{+-} \sin\delta \\ 0 \\ g_{++} \cos\delta \\ 0
%        \end{pmatrix}
%        \begin{pmatrix}
%            \displaystyle
%            0 \\ g_{-+} \sin\delta \\ 0 \\ g_{--} \cos\delta
%        \end{pmatrix}
%    \end{gather}
%    \endgroup

    \section{Parameters}
    Suppose there are 2 known frequencies
    $ \omega_1 $ and $ \omega_2 $ such that $ \omega_2 > \omega_1 $.
    All transition frequencies can be written
    as some linear combination of
    \begin{equation}
        \left\{
            1, \sqrt{a_+^2 + \sin^2 D}, a_- \sqrt{1 + \frac{\sin^2 D}{a_-^2}}
        \right\}.
    \end{equation}
    For 2 different frequencies $ f_m $ and $ f_n $,
    where $ f_n > f_m $,
    we must solve the system
    \begin{gather}
        \frac{f_m}{f_n} = \frac{\omega_1}{\omega_2}
        \\
        \wi f_n = \omega_2.
    \end{gather}
    Suppose $ p_1 $, $ p_2 $, and $ p_3 $ are the coefficients
    used to represent $ f_m $
    and $ p_4 $, $ p_5 $, and $ p_6 $ for $ f_n $.
    Then, we obtain solution
    \begin{equation}
        \Qty{a_- \sqrt{1 + \frac{\sin^2 D}{a_-^2}}, \sqrt{a_+^2 + \sin^2 D}}
        = \Qty{-c_1, c_2},
    \end{equation}
    where
    \begin{align}
        c_1 = \frac{\omega_1 p_5 - \omega_2 p_2 + \wi \Qty{p_2 p_4 - p_1 p_5}}
                   {\wi \Qty{p_2 p_6 - p_3 p_5}}
        \\
        c_2 = \frac{\omega_1 p_6 - \omega_2 p_3 + \wi \Qty{p_3 p_4 - p_1 p_6}}
                   {\wi \Qty{p_2 p_6 - p_3 p_5}}.
    \end{align}
    These then give solutions
    \begin{align}
        a_+ = \sqrt{c_2^2 - \sin^2 D}
        \\
        a_- = \pm \sqrt{c_1^2 - \sin^2 D}.
    \end{align}
    Using the definitions of $ a_+ $ and $ a_- $,
    we obtain
    \begin{equation}
        \alpha = \frac{1}{2} \sqrt{2 \Qty{c_1^2 + c_2^2 - 2}}
        \label{eq:alpha-sol}
    \end{equation}
    \begin{equation}
        \cos D = \pm \frac{c_1^2 - c_2^2}{2 \sqrt{2 \Qty{c_1^2 + c_2^2 - 2}}}.
        \label{eq:cosD-sol}
    \end{equation}

    \section{Visualizations}
    Since a mixture of the eigenstates of $ I_z $ occur,
    more transitions than those following the usual transition rules
    can be anticipated.
    In a zero-field NMR experiment, an RF pulse will be used to excite
    the material and the magnetic moment is the observable.
    So, to visualize the allowed transitions,
    we calculate $ \langle I_y \rangle $,
    which corresponds to an RF pulse in the $ x $ direction
    for an internal field in the $ z $ direction.
    The normalized values are used as colors and shown
    in \Fig{magmom}.
    Eigenvalues and all transitions, allowed or forbidden,
    are visualized and labeled according to their respective
    eigenstates in \Fig{transitions}.

    From \Fig{transitions}, it is clear which transitions
    are larger than the others.
    In particular, if we take measured values
    In particular for LaFeAsO,
    we may use measured values
    from Carretta et al.\ \cite{Carretta2019}
    $ \omega_1 = \SI{11.8}{MHz} $,
    $ \omega_2 = \SI{20.6}{MHz} $,
    and measured internal field $ H_\mathrm{i} = \SI{1.60}{T} $
    \cite{Mukada2009}
    on the As site,
    to find a solution to \Eq{alpha-sol} and \Eq{cosD-sol}.
    This may be done by sweeping through all possible combinations
    of transitions and removing non-physical solutions
    as well as those without a measurable magnetic moment.
    In doing so, a single solutions emerges
    with values $ \alpha = 0.382 $ and $ \cos D = 0.992 $,
    which correspond to a $ V_{zz} = \SI{0.374e21}{V/m^2} $
    and $ \eta = \SI{0.224}{} $.
    The frequencies
    $ \omega_1 $ and $ \omega_2 $ correspond to transitions
    $ -1/2'' \to 1/2'' $ and $ 1/2'' \to 3/2'' $, respectively.

    \begin{figure}[!htbp]
        \centering
        \includegraphics[scale=1.25]{energy_trans.jpg}
        \caption{Energy and transitions as a function of $ \alpha $.}
        \label{fig:transitions}
    \end{figure}
    \begin{figure}[!htbp]
        \centering
        \includegraphics[scale=1.5]{magmom.jpg}
        \caption{Transitions as a function of alpha
                 colored according to magnetic moment.
                 This assumes an RF pulse in the $ x $-direction.
                 The $ \langle I_x \rangle $ plot is the imaginary
                 component and $ \langle I_y \rangle $
                 is the real component.}
        \label{fig:magmom}
    \end{figure}



    \newpage

    \appendix
    \section{Appendix}
    \label{sec:appendix}
    The following are the matrix elements of the Zeeman Hamiltonian
    in the quadrupolar basis.
    A factor of 1/2 is assumed in front of all terms.
    \begin{align}
        \bra{+3/2'}I_z\ket{+3/2'} &= 3\cos^2\delta - \sin^2\delta
        \\
        \bra{+1/2'}I_z\ket{+3/2'} &= 0
        \\
        \bra{-1/2'}I_z\ket{+3/2'} &= -3\cos\delta\sin\delta - \cos\delta\sin\delta
        \\
        \bra{-3/2'}I_z\ket{+3/2'} &= 0
        \\
        \notag
        \\
        \bra{+3/2'}I_z\ket{+1/2'} &= 0
        \\
        \bra{+1/2'}I_z\ket{+1/2'} &= \cos^2\delta - 3\sin^2\delta
        \\
        \bra{-1/2'}I_z\ket{+1/2'} &= 0
        \\
        \bra{-3/2'}I_z\ket{+1/2'} &= 3 \cos\delta\sin\delta + \cos\delta\sin\delta
        \\
        \notag
        \\
        \bra{+3/2'}I_z\ket{-1/2'} &= -\cos\delta\sin\delta - 3\cos\delta\sin\delta
        \\
        \bra{+1/2'}I_z\ket{-1/2'} &= 0
        \\
        \bra{-1/2'}I_z\ket{-1/2'} &= -\cos^2\delta + 3\sin^2\delta
        \\
        \bra{-3/2'}I_z\ket{-1/2'} &= 0
        \\
        \notag
        \\
        \bra{+3/2'}I_z\ket{-3/2'} &= 0
        \\
        \bra{+1/2'}I_z\ket{-3/2'} &= 3\cos\delta\sin\delta + \cos\delta\sin\delta
        \\
        \bra{-1/2'}I_z\ket{-3/2'} &= 0
        \\
        \bra{-3/2'}I_z\ket{-3/2'} &= -3\cos^2\delta + \sin^2\delta
    \end{align}


    \newpage

    \bibliographystyle{plain}
    \bibliography{bib}


\end{document}
