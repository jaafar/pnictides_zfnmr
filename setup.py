from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Using zero-field NMR to investigate pnictide superconductors',
    author='Jaafar Ansari',
    license='MIT',
)
